// change the font size and style
function mouse_change_font(sel, val){
    if(sel == 0) text_face = val;    
    else text_size = val;

    ctx.font = "normal normal "+ text_size + "px " + text_face;
}