// load the image in local
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#uploadImage')
                .attr('src', e.target.result)
                .width(30)
                .height(30);
        };
        reader.readAsDataURL(input.files[0]);
    }
}