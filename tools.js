function start(){
    cur = document.getElementById("main");
    cursor_property = 0;
    can = document.getElementById("art");
    ctx = can.getContext("2d");
    drawing = false;
    // text
    text = document.getElementById("textBoard");
    //color board
    color = document.getElementById("colorBoard");
    //slider
    sld = document.getElementById("slideBar");
    ctx.lineWidth = sld.value;
    //undo
    prevImgData_id = -1;
    backvalue = -1;
    backcheck = false;
    remains = ctx.getImageData(0, 0, can.width, can.height);
    // download
    dw = document.getElementById("download");
    // upload image
    img = new Image();
}
function getRads (degrees) { return (Math.PI * degrees) / 180; }
function store_data(){
    let imgD = ctx.getImageData(0, 0, can.width, can.height);
    backvalue = -1;
    backcheck = false;
    if(prevImgData_id == prevIDmax){   
        for(var i=0;i<prevIDmax;i++){
            prevImgData[i] = prevImgData[i+1];
        }
        prevImgData[prevIDmax] = imgD;
    }
    else{
        prevImgData_id++;
        prevImgData[prevImgData_id] = imgD;
    }
}
