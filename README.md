# Software Studio 2020 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Name of widgets                                  | 1~5%     | N         |


---

# How to use 
- **Color Select**
    We can use the color board to select favor color to draw and type text. (initial to black)
    
- **Pen & Eraser**
    Click the button "Pen" to start drawing. 
    Click the button "Eraser" to erase whatever you want.
    
- **Brush shapes**
    The slide bar below the color board is a slider which can change the brush type (small, middle, or thick)(totally 9 types)

- **Text**
    The "10, 20, 30, 40" are text size buttons, and "Arial, Courier, Serif" are text style buttons. The followings are the step for texting:
     **Step1.**
     Select the text size and text style you prefer.
     **Step2.**
     Type the text you want in the text bar.
     **Step3.**
     Click the button "TEXT", and move your cursor to the position that you want to text on it.
     **Step4.**
     Click the left button of your mouse, then you can see the text is now on the canvas.
     
- **Circle, Triangle, Rectangle, Line**
    You can click these four shape buttons which you favor, and just click you mouse and drag it. When the left button of you mouse is up, the shape will draw on the canvas.

- **Upload**
    You can upload your image from local to draw on it by the following steps:
    **Step1.**
    Press the button "選擇檔案" and choose the image you want to upload.
    **Step2.**
    Move your cursor to the position that you want to place the picture.
    **Step.3**
    Click the left button of your mouse, the the picture will paste on the canvas.
    
- **Download**
    Press the button "Download", then you can download the image on your canvas to local.
    
- **Undo & Redo**
    Click "Undo" button, then your canvas will go back to the last step.
    Click "Redo" button, then your canvas will go to the next step.
- **Reset**
    Press the button "Reset", then the canvas will reset.
# Function description
## lib.js
```javascript=
var cur;
var cursor_property;
var ctx;
var can;
var drawing;
var s_x, s_y;
// color board
var color;
// text
var text;
var text_face;
var text_size;
// slide
var sld;
// polygon
var remains;
// undo
var prevImgData = [];
var prevImgData_id;
const prevIDmax = 30;
var backvalue;
var backcheck;
// download
var dw;
// upload img
var img;
```
Most parameter defined here.
## mouse.js
```javascript=
function mouse_click(draw_event) {
    switch(draw_event) {
        case 1:
        case 2:
        case 3:
            .
            .
            .
        case 11:
        case 12:
    }
} 
```
Used to perform corresponding movement when the mouse is be clicked.
- **draw_event**
represent the which button event occur
- **case**
1 for pen event,
2 for eraser, 
3 for text, 
4 for circle, 
5 for triangle, 
6 for rectangle, 
7 for line, 
8 for undo, 
9 for redo, 
10 for reset, 
11 for download, 
12 for upload.
```javascript=
function mouse_down(event){
    switch(cursor_property):
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
        case 12:
}
```
Used to deal with the situaltion when the mouse button is being pressed.
-**cursor_property**
represent the cursor's situation (correspnding to each case).
-**case**
same with above.
```javascript=
function mouse_move(event){
    switch(cursor_property):
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
}
```
Used to deal with the situaltion when the mouse  is being pressed and moving. The case is only for tools such as pen, eraser, circle, triangle, rectangle and line which are all drawing tool.
-**cursor_property**
represent the cursor's situation (correspnding to each case).
-**case**
same with above.
```javascript=
function mouse_up(event){
    switch(cursor_property):
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
}
```
Used to deal with the situaltion when the mouse button is up. The case is only for tools such as pen, eraser, circle, triangle, rectangle and line which are all drawing tool.
-**cursor_property**
represent the cursor's situation (correspnding to each case).
-**case**
same with above.
```javascript=
function brush_style(){
    ctx.lineWidth = sld.value
}
```
Used to perfrom the changing of brush style(get the event by the slide bar).

## color.js
```javascript=
function Cur_Color(event){
    switch(cursor_property):
        case 1:
            ctx.globalCompositeOperation = "source-over";
            ctx.strokeStyle = color.value;
            ctx.fillStyle = color.value;
            break;
        case 2:
            ctx.globalCompositeOperation = "destination-out";
            ctx.strokeStyle = color.value;
            ctx.fillStyle = color.value;
            break;
        case 3:
            ctx.globalCompositeOperation = "source-over";
            ctx.strokeStyle = color.value;
            ctx.fillStyle = color.value;
            break;
        case 4:
            ctx.globalCompositeOperation = "source-over";
            ctx.strokeStyle = color.value;
            ctx.fillStyle = color.value;
            break;
        case 5:
            ctx.globalCompositeOperation = "source-over";
            ctx.strokeStyle = color.value;
            ctx.fillStyle = color.value;
            break;
        case 6:
            ctx.globalCompositeOperation = "source-over";
            ctx.strokeStyle = color.value;
            ctx.fillStyle = color.value;
            break;
        case 7:
            ctx.globalCompositeOperation = "source-over";
            ctx.strokeStyle = color.value;
            ctx.fillStyle = color.value;
            break;
        default:
            break;
}
```
Used to change the color when we are drawing, or texting. Execpt case 2 use "destination-out" to erase canvas, other cases use "source-over" to paint on canvas.
## font.js
```javascript=
function mouse_change_font(sel, val){
    if(sel == 0) text_face = val;
    else text_size = val;
    
    ctx.font = "normal normal" + text_size + "px" + text_face
}
```
Used to perform the changing of text size and style.
If the value of sel equal to 0 that means the parameter val is the value of text style.
If not equal to 0 that means the parameter val is the value of text size.
## internet.js
```javascript=
function readURL(input){
    if(input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#uploadImage')
                 .attr('src', e.target.result);
                 .width(30);
                 .height(30);
        };
        reader.readAsDataURL(input.files[0]);
    }
}
```
Used to upload the image we want from local to canvas.
## tools.js
```javascript=
function start(){
    ......
}
```
Used to initialization.
```javascript=
function getRads(degree){
    return (MATH*PI, can.height);
}
```
Used to return the current radias of the circle we are now drawing.
```javascript=
function store_data(){
   ......
}
```
Used to store the data(situation, drawing,...... etc) on the canvas.
# Gitlab page link

    https://106032053.gitlab.io/AS_01_WebCanvas

# Others (Optional)
這次的作業結合了之前上課好多的內容，我原本連處理html跟css都用得很困難．一開始看到這個作業真的被嚇到了，因此上網找了好多跟小畫家有關的教學文章，也練習了很多function的寫法以及內容．在過程中常常用一個功能就用好久，像是我原本已經完成了，但發先download功能沒辦法使用，結果改善它就花了快一整天的時間......，不過這個作業真的很紮實，讓我對html, css, js的實作又更加了解了！

