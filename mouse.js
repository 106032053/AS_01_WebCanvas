/* Choose the size of brush */
function brush_style(){
    ctx.lineWidth = sld.value;
}
/* mouse click */
function mouse_click(draw_event){
    switch(draw_event) {
        // drawtool - pen
        case 1:
            if(cursor_property == 1){
                cursor_property = 0;
            }
            else{
                cursor_property = 1;
            }
            break;
        // drawtool - eraser
        case 2:
            if(cursor_property == 2){
                cursor_property = 0;
            }
            else{
                cursor_property = 2;
            }
            break;
        // drawtool - text
        case 3:
            if(cursor_property == 3){
                cursor_property = 0;
            }
            else{
                cursor_property = 3;
            }
            break;
        // shape - circle   
        case 4:
            if(cursor_property == 4){
                cursor_property = 0;
            }
            else{
                cursor_property = 4;
            }
            break;
        // shape - triangle
        case 5:
            if(cursor_property == 5){
                cursor_property = 0;
            }
            else{
                cursor_property = 5;
            }
            break;
        // shape - rectangle
        case 6:
            if(cursor_property == 6){
                cursor_property = 0;
            }
            else{
                cursor_property = 6;
            }
            break;
        // shape - line
        case 7:
            if(cursor_property == 7){
                cursor_property = 0;
            }
            else{
                cursor_property = 7;
            }
            break;
        // undo
        case 8:
            if(prevImgData_id >= 0){
                if(backvalue == -1){store_data(); backvalue = prevImgData_id;}
                if(backcheck == false){prevImgData_id--; backcheck = true;}
                ctx.putImageData(prevImgData[prevImgData_id], 0, 0);
                remains = ctx.getImageData(0, 0, can.width, can.height);
                prevImgData_id--;
            }
            break;
        // redo
        case 9:
            if(prevImgData_id < backvalue){
                if(backcheck == true){prevImgData_id++; backcheck = false;}
                prevImgData_id++;
                ctx.putImageData(prevImgData[prevImgData_id], 0, 0);
                remains = ctx.getImageData(0, 0, can.width, can.height);
            }
            break;
        // reset
        case 10:
            ctx.clearRect(0, 0, can.width, can.height);
            remains = ctx.getImageData(0, 0, can.width, can.height);
            prevImgData_id = -1;
            backvalue = -1;
            backcheck = false;
            break;
        // download
        case 11:
            dw.download = "image.png";
            dw.href = can.toDataURL("image/png").replace(/^data:image\/[^;]/, 'data:application/octet-stream');
            break;
        // upload
        case 12:
            cursor_property = 12;
            break;
    }
}

/* mouse down */
function mouse_down(event){
    switch(cursor_property) {
        // drawtool = pen
        case 1:
            ctx.beginPath();
            ctx.moveTo(event.offsetX, event.offsetY);
            drawing = true;
            store_data();
            Cur_Color();
            break;
        // drawtool - eraser
        case 2:
            ctx.beginPath();
            ctx.moveTo(event.offsetX, event.offsetY);
            drawing = true;
            store_data();
            Cur_Color();
            break;
        // drawtool - text
        case 3:
            store_data();
            Cur_Color();
            ctx.fillText(text.value,event.offsetX,event.offsetY);
            cursor_property = 0;
            break;
        // shape - circle
        case 4:
            s_x = event.offsetX;
            s_y = event.offsetY;
            drawing = true;
            store_data();
            Cur_Color();
            break;
        // shape - triangle
        case 5:
            s_x = event.offsetX;
            s_y = event.offsetY;
            drawing = true;
            store_data();
            Cur_Color();
            break;
        // shape - rectangle
        case 6:
            s_x = event.offsetX;
            s_y = event.offsetY;
            drawing = true;
            store_data();
            Cur_Color();
            break;
        // shape - line
        case 7:
            s_x = event.offsetX;
            s_y = event.offsetY;
            drawing = true;
            store_data();
            Cur_Color();
            break;
        case 12:
            var img = document.getElementById("uploadImage");
            store_data();
            ctx.drawImage(img, event.offsetX, event.offsetY);
            break;
        default:
            break;
    }
}
function mouse_move(event) {
    switch(cursor_property) {
        // drawtool - pen
        case 1:
            if(drawing == true){
                ctx.lineTo(event.offsetX, event.offsetY);
                ctx.stroke();
            }
            break;
        // drawtool - eraser
        case 2:
            if(drawing == true){
                ctx.lineTo(event.offsetX, event.offsetY);
                ctx.stroke();
            }
            break;
        // shape - circle
        case 4:
            if(drawing == true){
                ctx.clearRect(0,0,can.width,can.height);
                ctx.putImageData(remains, 0, 0);
                ctx.beginPath();
                var radius = ((event.offsetX-s_x)**2+(event.offsetY-s_y)**2)**(1/2);
                ctx.arc(s_x, s_y, radius, getRads(0), getRads(360));
                ctx.stroke();
                ctx.fill();
            }
            break;
        // shape - triangle
        case 5:
            if(drawing == true){
                ctx.clearRect(0,0,can.width,can.height);
                ctx.putImageData(remains, 0, 0);
                ctx.beginPath();
                let diff = s_y-(event.offsetY);
                ctx.moveTo(s_x, s_y);
                ctx.lineTo(event.offsetX, event.offsetY);
                ctx.lineTo(event.offsetX, s_y+diff);
                ctx.fill();
            }
            break;
        // shape - rectangle
        case 6:
            if(drawing == true){
                //clear -> draw
                ctx.clearRect(0,0,can.width,can.height);
                ctx.putImageData(remains, 0, 0);
                ctx.fillRect(s_x, s_y, event.offsetX-s_x, event.offsetY-s_y);
            }
            break;
        // shape - line
        case 7:
            if(drawing == true){
                ctx.clearRect(0,0,can.width,can.height);
                ctx.putImageData(remains, 0, 0);
                ctx.beginPath();
                ctx.moveTo(s_x, s_y);
                ctx.lineTo(event.offsetX, event.offsetY);
                ctx.stroke();
            }
            break;
        default:
            break;
    }
}
function mouse_up(){
    switch(cursor_property) {
        // drawtool - pen
        case 1:
            drawing = false;
            break;
        // drawtool - eraser
        case 2:
            drawing = false;
            break;
        // shape - circle
        case 4:
            drawing = false;
            ctx.clearRect(0,0,can.width,can.height);
            ctx.putImageData(remains, 0, 0);
            ctx.beginPath();
            var radius = ((event.offsetX-s_x)**2+(event.offsetY-s_y)**2)**(1/2);
            ctx.arc(s_x, s_y, radius, getRads(0), getRads(360));
            ctx.stroke();
            ctx.fill();
            break;
        // shape - triangle
        case 5:
            drawing = false;
            ctx.clearRect(0,0,can.width,can.height);
            ctx.putImageData(remains, 0, 0);
            ctx.beginPath();
            let diff = s_y-(event.offsetY);
            ctx.moveTo(s_x, s_y);
            ctx.lineTo(event.offsetX, event.offsetY);
            ctx.lineTo(event.offsetX, s_y+diff);
            ctx.fill();
            break;
        // shape - rectangle
        case 6:
            drawing = false;
            ctx.clearRect(0,0,can.width,can.height);
            ctx.putImageData(remains, 0, 0);
            ctx.fillRect(s_x, s_y, event.offsetX-s_x, event.offsetY-s_y);
            break;
        // shape - line
        case 7:
            drawing = false;
            ctx.clearRect(0,0,can.width,can.height);
            ctx.putImageData(remains, 0, 0);
            ctx.beginPath();
            ctx.moveTo(s_x, s_y);
            ctx.lineTo(event.offsetX, event.offsetY);
            ctx.stroke();
            break;
        default:
            break;
    }
    remains = ctx.getImageData(0, 0, can.width, can.height);
}